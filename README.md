
# DNSSEC helper scripts

Recent enough versions of BIND perform automatic zone resigning as
well as key rotation, assuming that keys have been pregenerated. These
helper scripts intend to make it easier to work with BIND's
*dnssec-keygen*, etc.

## zskregen

Assuming an existing (zone-signing) key the *zskregen* script will
generate follow up keys, as well as correspondingly modify the
Inactive date for the original key. All this in a temporary directory,
keeping the real key directory safe.

### Usage

    $ ./zskregen --help
    Usage:
      ./zskregen [options] keyfile
      ./zskregen --help
    
    options:
     -h, --help
        Print this help message.
     -n, --number=INTEGER
        Number of new keys to generate. Defaults to 6. 
     -l, --lifetime=INTEGER
        Lifetime, in number of months, per key. Defaults to 3.
     --allow-ksk
        Also work on key-signing keys.
     --real-random
        Use /dev/random for entropy, instead of /dev/urandom.

### Example

    $ dnssec-keygen -a 8 -b 2048 -f KSK example.net
    Kexample.net.+008+34009
    $ dnssec-keygen -a 8 -b 1024 example.net
    Kexample.net.+008+06081
    $ zskregen -n 3 Kexample.net.+008+06081
    The new keys are available under: /tmp/tmpIud5b7

## keypruner

While BIND does a great job at rotating DNSSEC keys it will not
automatically remove deleted keys from the filesystem. That is what
this script helps out with. It can also perform the same pruning for
keys which have yet to be published.

### Usage

    $ ./keypruner --help
    Usage:
      ./keypruner [options] [/path/to/directory]
      ./keypruner --help
    
    options:
     -h, --help
        Print this help message.
     --force
        Actually prune files, instead of just listing the pruneable ones.
     --future
        Instead pruning deleted keys, prune unpublished keys.
     --only-ksk / --only-zsk
        Only prune key-signing keys, alt only zone-signing keys.

### Example

    $ keypruner /etc/bind/keys/example.org/
    Pruneable key files:
    
      /etc/bind/keys/example.org/Kexample.org.+008+62940.{key,private} (Delete: 2015-05-10 10:10)
      /etc/bind/keys/example.org/Kexample.org.+008+15720.{key,private} (Delete: 2015-08-10 10:10)
      /etc/bind/keys/example.org/Kexample.org.+008+49824.{key,private} (Delete: 2015-11-10 10:10)
    
    To actually prune these files, use the --force.

## keys2table

To better keep track of when what rotation will happen the
*keys2table* script can parse the keys in a directory, and provide a
table with all the dates. The output can either be as an ascii table
or as mediawiki markup.

### Usage

    $ ./keys2table --help
    Usage:
      ./keys2table [options] [/path/to/directory]
      ./keys2table --help
    
    options:
     -h, --help
        Print this help message.
     --only-ksk / --only-zsk
        Only display key-signing keys, alt only zone-signing keys.
     -w, --wiki
        Instead of ascii table, output mediawiki markup.

### Example

    $ keys2table
    
     ------------------------------------------------------------------------------------------
    | example.net                                                                              |
     ------------------------------------------------------------------------------------------
    | KeyID | Type | Publish          | Activate         | Inactive         | Delete           |
     ------------------------------------------------------------------------------------------
    | 34009 | KSK  | 2015-03-28 06:45 | 2015-03-28 06:45 |                  |                  |
     ------------------------------------------------------------------------------------------
    | 06081 | ZSK  | 2015-03-28 06:46 | 2015-03-28 06:46 | 2015-06-26 06:48 | 2015-07-26 06:48 |
     ------------------------------------------------------------------------------------------
    | 17649 | ZSK  | 2015-05-27 06:48 | 2015-06-26 06:48 | 2015-09-24 06:48 | 2015-10-24 06:48 |
     ------------------------------------------------------------------------------------------
    | 29025 | ZSK  | 2015-08-25 06:48 | 2015-09-24 06:48 | 2015-12-23 06:48 | 2016-01-22 06:48 |
     ------------------------------------------------------------------------------------------
    | 07605 | ZSK  | 2015-11-23 06:48 | 2015-12-23 06:48 |                  |                  |
     ------------------------------------------------------------------------------------------
