
import datetime
import os
import re
import subprocess
import sys
from glob import glob
from operator import itemgetter


def bailout(exitmsg):
    sys.stderr.write(exitmsg + '\n')
    sys.exit(1)


def eo(em):
    sys.stderr.write(str(em) + '\n')


def getfiles(dirpath):
    fullpath = os.path.abspath(dirpath)
    pattern = os.path.join(fullpath, 'K*.key')
    list = glob(pattern)
    return list


def nameparse(name):
    result = {}
    rexet = re.match(r'(K(.+)\.\+(\d{3})\+(\d{5}))\.key$', name)
    if rexet:
        result['name'] = rexet.group(1)
        result['zone'] = rexet.group(2)
        result['alg'] = rexet.group(3)
        result['keyid'] = rexet.group(4)
        return result
    else:
        raise ValueError('Failed to recognize {}'.format(name))


def getkeycont(filepath):
    result = {'Publish': '', 'Activate': '', 'Inactive': '', 'Delete': ''}
    pubkey = open(filepath, 'r')
    for line in pubkey:
        rexet = re.match(r'; (\w+): (\d{14})', line)
        if rexet:
            result[rexet.group(1)] = dateparse(rexet.group(2))
        elif 'IN DNSKEY 256' in line:
            result['role'] = 'ZSK'
        elif 'IN DNSKEY 257' in line:
            result['role'] = 'KSK'
    return result


def keyparse(filepath):
    result = {}
    filename = os.path.basename(filepath)
    try:
        result.update(nameparse(filename))
    except ValueError as ve:
        sys.stderr.write('{}, skipping.\n'.format(str(ve)))
        return False
    try:
        result.update(getkeycont(filepath))
    except IOError:
        sys.stderr.write('Failed to read {}, skipping\n'.format(filepath))
        return False
    return result


def zonesplit(keylist, sort=False):
    result = {}
    zones = set([])
    for keydict in keylist:
        result[keydict['zone']] = []
        zones.add(keydict['zone'])
    for keydict in keylist:
        result[keydict['zone']].append(keydict)
    if sort:
        for zone in zones:
            result[zone].sort(key=itemgetter('Activate'))
            result[zone].sort(key=itemgetter('role'))
    return result


def verify_requirements(keydict, reqs):
    if not reqs:
        return True
    for test in reqs.keys():
        if keydict[test] != reqs[test]:
            return False
    return True


def process_keydir(path, reqs={}):
    keyfiles = getfiles(path)
    if not keyfiles:
        raise ValueError('Found no key files in {}'.format(path))
    keycontent = []
    for keyfile in keyfiles:
        keydata = keyparse(keyfile)
        if keydata:
            if verify_requirements(keydata, reqs):
                keycontent.append(keydata)
    if not keycontent:
        raise ValueError('Found no parsable key files in {}'.format(path))
    return zonesplit(keycontent, sort=True)


def dateparse(compact):
    rexet = re.match(r'(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})', compact)
    if rexet:
        dy, dm, dd, th, tm, ts = map(int, rexet.groups())
        return datetime.datetime(dy, dm, dd, th, tm, ts)
    else:
        raise ValueError('Found no date to parse')


def namenormalize(inputname):
    result = {}
    thebase = os.path.basename(inputname)
    thedir = os.path.dirname(inputname)
    result['path'] = os.path.abspath(thedir)
    result['name'], n = re.subn(r'^(K.+\+\d+\+\d+)(\.(key|private))?$', r'\1', thebase)
    if n != 1:
        raise ValueError("{} doesn't appear to be a proper key name".format(inputname))
    result['pub'] = result['name'] + '.key'
    result['priv'] = result['name'] + '.private'
    return result


def diglookup(zonename, ktype, regex, extraopts=[]):
    uidset = set([])
    digcmd = ['dig']
    digopts = ['+nocl', '+nottlid', '+nocmd', '+nocomments', '+nostats',
               '+noquestion', '+noauthority', '+noadditional']
    digopts.extend(extraopts)
    digcmd.extend(digopts)
    digcmd.extend([zonename, ktype])
    rawdig = subprocess.check_output(digcmd).rstrip()
    rexet = re.compile(regex)
    for line in rawdig.split('\n'):
        regres = rexet.match(line)
        if regres:
            uidset.add(regres.group(1))
    return uidset


def dsuidfromdns(zonename):
    dsregex = r'{}\.\s+DS\s+(\d+)\s+.+'.format(zonename)
    return diglookup(zonename, 'DS', dsregex)


def keyuidfromdns(zonename):
    keyregex = r'{}\.\s+DNSKEY.+key id = ([0-9]+)\s*$'.format(zonename)
    return diglookup(zonename, 'DNSKEY', keyregex, ['+rrcomments'])
