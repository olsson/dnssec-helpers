#!/usr/bin/env python

import dnssec_key_helpers as dkh
import getopt
import os
import re
import sys


def usage():
    print 'Usage:'
    print '  {} [options] [/path/to/directory]'.format(sys.argv[0])
    print '  {} --help'.format(sys.argv[0])


def help():
    tfm = '''
options:
 -h, --help
    Print this help message.
 --only-ksk / --only-zsk
    Only display key-signing keys, alt only zone-signing keys.
 -w, --wiki
    Instead of ascii table, output mediawiki markup.
'''
    usage()
    print tfm


def getconfig(commandline):
    try:
        opts, args = getopt.getopt(commandline, 'hw', ['help', 'only-ksk', 'only-zsk', 'wiki'])
    except getopt.GetoptError as GoE:
        dkh.eo(GoE)
        usage()
        sys.exit(1)
    wiki = False
    onlyksk, onlyzsk = False, False
    reqs = {}
    for opt, arg in opts:
        if opt in ['-h', '--help']:
            help()
            sys.exit(0)
        elif opt in ['-w', '--wiki']:
            wiki = True
        elif opt in ['--only-ksk']:
            onlyksk = True
        elif opt in ['--only-zsk']:
            onlyzsk = True
    if onlyksk and onlyzsk:
        dkh.bailout('--only-ksk and --only-zsk are mutually exclusive.')
    elif onlyksk:
        reqs['role'] = 'KSK'
    elif onlyzsk:
        reqs['role'] = 'ZSK'
    if not args:
        dir = '.'
    elif len(args) == 1:
        dir = args[0]
    else:
        usage()
        sys.exit(1)
    return dir, wiki, reqs


def s(n):
    return ' ' * n


def dateprint(dtobj, padd=False):
    try:
        return dtobj.strftime('%Y-%m-%d %H:%M')
    except AttributeError:
        if padd:
            return '                '
        else:
            return ''


def tabledraw(keydict):
    for zone in keydict:
        line = ' ' + '-' * 90
        zp = (89 - len(zone)) * ' '
        print ''
        print line
        print '| {}{}|'.format(zone, zp)
        print line
        print '| KeyID | Type | Publish{}| Activate{}| Inactive{}| Delete{}|'.format(s(10), s(9), s(9), s(11))
        print line
        for entry in keydict[zone]:
            kid = entry['keyid']
            typ = entry['role']
            publ = dateprint(entry['Publish'], padd=True)
            acti = dateprint(entry['Activate'], padd=True)
            iact = dateprint(entry['Inactive'], padd=True)
            dele = dateprint(entry['Delete'], padd=True)
            print '| {} | {}  | {} | {} | {} | {} |'.format(kid, typ, publ, acti, iact, dele)
            print line


def wikidraw(keydict):
    for zone in keydict:
        print '{| class="wikitable"'
        print '|+ {}'.format(zone)
        print '! KeyID'
        print '! Type'
        print '! Publish'
        print '! Activate'
        print '! Inactive'
        print '! Delete'
        for entry in keydict[zone]:
            print '|-'
            print '| {}'.format(entry['keyid'])
            print '| {}'.format(entry['role'])
            print '| {}'.format(dateprint(entry['Publish']))
            print '| {}'.format(dateprint(entry['Activate']))
            print '| {}'.format(dateprint(entry['Inactive']))
            print '| {}'.format(dateprint(entry['Delete']))
        print '|}'


if __name__ == "__main__":
    inputdir, usewiki, requirements = getconfig(sys.argv[1:])
    try:
        keysperzone = dkh.process_keydir(inputdir, requirements)
    except ValueError as ve:
        dkh.bailout('{}, exiting.'.format(str(ve)))

    if usewiki:
        wikidraw(keysperzone)
    else:
        tabledraw(keysperzone)
